package com.example.demolab2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demolab2Application {

    public static void main(String[] args) {
        SpringApplication.run(Demolab2Application.class, args);
    }

}
